function listchucnang (){
    // add SV
    this.themSV = function(DSSV, id){
        let content = ''
        DSSV.forEach(sv =>{
            content += 
            `
            <tr>
            <td>${sv.maSV}</td>
            <td>${sv.tenSV}</td>
            <td>${sv.emailSV}</td>
            <td>${sv.date}</td>
            <td>${sv.khoahoc}</td>
            <td>${sv.DTB}</td>
            <td>
            <button onclick="chucnangxoa('${sv.maSV}')" class="btn btn-danger">Xóa</button>
            <button onclick="chucnangsua('${sv.maSV}')" class="btn btn-warning btn">Sửa</button>
            </td>
            </tr>
           `
        });
        var ketqua = document.getElementById(id).innerHTML = content
        return ketqua
    }
    
    // xoa SV
    this.XoaSV = function(DSSV, MaSv){
        for(var i = 0; i < DSSV.length; i++) {
            if (DSSV[i].maSV == MaSv) {
                DSSV.splice(i, 1)
            }
        } 
        return DSSV
    }

    // sua SV
    this.SuaSV = function(DSSV, MaSv){
        for(var i = 0; i < DSSV.length; i++){
          if(DSSV[i].maSV == MaSv){
             document.getElementById('txtMaSV').value = DSSV[i].maSV
             document.getElementById('txtMaSV').disabled = true
             document.getElementById('txtTenSV').value = DSSV[i].tenSV
             document.getElementById('txtEmail').value = DSSV[i].emailSV
             document.getElementById('txtPass').value = DSSV[i].matKhauSV
             document.getElementById('date').value = DSSV[i].date
             document.getElementById('khoahoc').value = DSSV[i].khoahoc
             document.getElementById('txtDiemToan').value = DSSV[i].diemToan
             document.getElementById('txtDiemLy').value = DSSV[i].diemLy
             document.getElementById('txtDiemHoa').value = DSSV[i].diemHoa
          }
        }
    }
    // search 
    this.SearchViTri = function(DSSV, MaSv){
        let viTri = DSSV.findIndex(function (item) {
            return item.maSV == MaSv
        })
        return viTri
    };
}