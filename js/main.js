// arrDSSV
var arrDSSV = [];

// danh sach chuc nang
let ChucNang = new listchucnang()

// Json
let dataJson = localStorage.getItem('DSSV')

getEL.innerHTML = render(DSSV);

if(dataJson != null){
  let dataArr = JSON.parse(dataJson)
  arrDSSV = dataArr.map(function(item){
    return new SinhVien (item.maSV, item.tenSV, item.emailSV, item.matKhauSV, item.date, item.khoahoc, item.diemToan, item.diemLy, item.diemHoa)
  });
  ChucNang.themSV(arrDSSV, 'tbodySinhVien')
}



// lay thong tin SV
function laythongtinSV(){
       var masv =  document.getElementById('txtMaSV').value; 
       var TenSV =  document.getElementById('txtTenSV').value;
       var Email =  document.getElementById('txtEmail').value; 
       var Password = document.getElementById('txtPass').value; 
       var ngay = document.getElementById('date').value; 
       var KhoaHoc =  document.getElementById('khoahoc').value; 
       var Toan =  document.getElementById('txtDiemToan').value;
       var Ly = document.getElementById('txtDiemLy').value; 
       var Hoa =  document.getElementById('txtDiemHoa').value; 
       
       return new SinhVien(masv, TenSV, Email, Password, ngay, KhoaHoc, Toan, Ly, Hoa)
}

function clearHTML() {
  document.getElementById("txtMaSV").value = "";
  document.getElementById("txtTenSV").value = "";
  document.getElementById("txtEmail").value = "";
  document.getElementById("txtPass").value = "";
  document.getElementById('date').value = "";
  document.getElementById('khoahoc').value = "";
  document.getElementById("txtDiemToan").value = "";
  document.getElementById("txtDiemLy").value = "";
  document.getElementById("txtDiemHoa").value = "";
}

function addSV(){
  let SV = laythongtinSV()
  arrDSSV.push(SV)
  ChucNang.themSV(arrDSSV, 'tbodySinhVien')
  Json(arrDSSV)
}

// Json
function Json(DSSV){
  localStorage.setItem('DSSV', JSON.stringify(DSSV))
}

// chuc nang xoa SV

function chucnangxoa(MaSv){
  ChucNang.themSV(ChucNang.XoaSV(arrDSSV, MaSv), 'tbodySinhVien')
  Json(arrDSSV)
}

// chuc nang chinh sua
function chucnangsua(id){
  ChucNang.SuaSV(arrDSSV, id)
}

// chuc nang cap nhat
function chucnangupdate(){
  var update = document.getElementById('txtMaSV').value;
  console.log(update);
  let viTri = ChucNang.SearchViTri(arrDSSV, update)
  if(viTri !=-1){
    arrDSSV[viTri].tenSV = document.getElementById('txtTenSV').value
    console.log(arrDSSV[viTri].TenSV);
    console.log(document.getElementById('txtTenSV').value)
    arrDSSV[viTri].emailSV = document.getElementById('txtEmail').value
    arrDSSV[viTri].matKhauSV = document.getElementById('txtPass').value
    arrDSSV[viTri].date = document.getElementById('date').value
    arrDSSV[viTri].khoahoc = document.getElementById('khoahoc').value
    arrDSSV[viTri].diemToan = document.getElementById('txtDiemToan').value
    arrDSSV[viTri].diemLy = document.getElementById('txtDiemLy').value
    arrDSSV[viTri].diemHoa = document.getElementById('txtDiemHoa').value
    arrDSSV[viTri].DTB = (arrDSSV[viTri].diemToan*1 + arrDSSV[viTri].diemLy*1 + arrDSSV[viTri].diemHoa * 1) /3
  }
  ChucNang.themSV(arrDSSV, 'tbodySinhVien')

}




function chucnangsearch(){
  var tim = document.getElementById('txtSearch').value;
  let arrSearch = []
  arrDSSV.forEach(function(sinhvien){
    if(sinhvien.maSV.toLowerCase().trim().search(tim.toLowerCase().trim()) != -1) 
    {
      arrSearch.push(sinhvien)
    }
  })
  ChucNang.themSV(arrSearch, 'tbodySinhVien')
}




