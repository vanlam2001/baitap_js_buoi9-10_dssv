function SinhVien(
  _maSV,
  _tenSV,
  _emailSV,
  _matKhauSV,
  _date,
  _khoahoc,
  _diemToan,
  _diemLy,
  _diemHoa
) {
  this.maSV = _maSV;
  this.tenSV = _tenSV;
  this.emailSV = _emailSV;
  this.matKhauSV = _matKhauSV;
  this.date = _date;
  this.khoahoc = _khoahoc;
  this.diemToan = _diemToan;
  this.diemLy = _diemLy;
  this.diemHoa = _diemHoa;
  this.DTB = this.TDTB()
}

SinhVien.prototype.TDTB = function(){
  this.DTB = (this.diemToan * 1 + this.diemLy * 1 + this.diemHoa * 1)/3
  return this.DTB;
}

